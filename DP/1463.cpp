#include <cstdio>
#include <algorithm>

using namespace std;

int main()
{
	int n;
	int d[1000001] = { 0, };
	scanf("%d", &n);

	for (int i = 2; i < 1000001; i++) {
		int s1 = 1000000, s2 = 1000000, s3 = 1000000;
		if (i % 3 == 0) s3 = d[i / 3];
		if (i % 2 == 0) s2 = d[i / 2];
		s1 = d[i - 1];
		d[i] = (min({ s3, s2, s1 }) + 1);
	}

	printf("%d", d[n]);
	return 0;
}