#define _CRT_SECURE_NO_WARNINGS

#include <cstdio>
#include <queue>

using namespace std;
int visit[100001] = { 0, };

int main()
{
	int n, k, depth = 1;
	scanf("%d %d", &n, &k);

	if (n == k) {
		printf("%d", 0);
		return 0;
	}

	queue<int> q;
	q.push(n);
	visit[n] = 1;

	while (!q.empty()) {
		int len = q.size();
		for (int i = 0; i < len; i++) {
			int next = q.front(); q.pop();
			// 문제의 수 범위 조건을 잘 읽을 것
			if (next - 1 >= 0 && visit[next - 1] == 0) {
				q.push(next - 1); 
				visit[next - 1] = 1;
			}
			if (next + 1 <= 100000 && visit[next + 1] == 0) {
				q.push(next + 1);
				visit[next + 1] = 1;
			}
			if (next * 2 <= 100000 && visit[next * 2] == 0) {
				q.push(next * 2);
				visit[next * 2] = 1;
			}
		}
		if (visit[k] == 1) break;
		depth++;
	}

	printf("%d", depth);
	return 0;
}