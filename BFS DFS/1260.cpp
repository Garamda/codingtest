#define _CRT_SECURE_NO_WARNINGS

#include <cstdio>
#include <vector>
#include <queue>
#include <algorithm>
using namespace std;

// 1. size 조심
vector<vector<int>> link(1001);
queue<int> q;
int visit[1001] = { 0, };


void dfs(int now)
{
	visit[now] = 1;
	printf("%d ", now);

	for (int i = 0; i < link[now].size(); ++i) {
		int next = link[now].at(i);
		if (visit[next] == 0) {
			dfs(next);
		}
	}
}

void bfs(int start)
{
	q.push(start);
	visit[start] = 1;

	while (!q.empty()) {
		int now = q.front();
		printf("%d ", now);
		q.pop();

		for (int i = 0; i < link[now].size(); ++i) {
			int next = link[now].at(i);
			if (visit[next] == 0) {
				// 2. 삽입 직후 visit 체크 (queue에 중복해서 들어오지 않는다)
				q.push(next);
				visit[next] = 1;
			}	
		}
	}	
}

int main()
{
	int n, m, v;

	scanf("%d %d %d", &n, &m, &v);

	for (int i = 0; i < m; ++i) {
		int a, b;
		scanf("%d %d", &a, &b);
		link[a].push_back(b);
		link[b].push_back(a);
	}

	// 3. 간선의 숫자 X, 정점의 숫자 O
	for (int i = 0; i < n; ++i) {
		sort(link[i].begin(), link[i].end());
	}
	
	dfs(v);
	printf("\n");

	// 4. size 조심
	for (int i = 0; i < 1001; ++i) {
		visit[i] = 0;
	}

	bfs(v);
	
	return 0;
}