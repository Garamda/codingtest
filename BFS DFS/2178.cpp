#define _CRT_SECURE_NO_WARNINGS

#include <cstdio>
#include <iostream>
#include <queue>
#include <string>
using namespace std;

int main()
{
	int a[102][102];
	int dx[4] = { -1, 0, 1, 0 }, dy[4] = { 0, 1, 0, -1};
	int visit[102][102] = { 0, };

	int n, m, depth = 1, flag = 0;
	scanf("%d %d", &n, &m);

	string str;
	for (int i = 1; i <= n; i++) {
		cin >> str;
		for (int j = 1; j <= m; j++) {
			// 1. 2차원 배열 외곽에 경계선을 둘렀을 때 index 조심
			a[i][j] = str[j - 1] - '0';
		}
	}

	queue<int> qx, qy;
	qx.push(1), qy.push(1);
	visit[1][1] = 1;

	while (!qx.empty()) {
		// 2. depth를 셀 땐 하나의 depth 단위로 queue의 element들을 분리하기 위해 for문으로 묶어내는 작업이 필요함
		int len = qx.size();
		for (int i = 0; i < len; i++) {
			int x = qx.front(); qx.pop();
			int y = qy.front(); qy.pop();

			for (int k = 0; k < 4; k++) {
				int nx = x + dx[k];
				int ny = y + dy[k];
				// 3. 배열 탐색시 if 문에 모든 조건이 들어갔는지 확인
				if (nx >= 1 && nx <= n && ny >= 1 && ny <= m && visit[nx][ny] == 0 && a[nx][ny] == 1) {
					if (nx == n && ny == m) {
						flag = 1;
						break;
					}
					qx.push(nx); qy.push(ny);
					visit[nx][ny] = 1;
				}
			}
			if (flag == 1) break;
		}
		depth++;
		if (flag == 1) break;
	}

	printf("%d", depth);
	return 0;
}